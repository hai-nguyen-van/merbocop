Merbocop: Merge-Request Bot Police 🤖
=====================================

Build
-----

Get dependencies:

    opam install base fmt cmdliner ezjsonm uri dune ocamlformat.0.15.1

and build:

    dune build merbocop.exe

CI Setup
--------

We use Gitlab's “Pipeline Schedules:”
<https://gitlab.com/smondet/merbocop/pipeline_schedules>.

A pipeline with `$full_cron_trigger == "true"` runs every 6 hours.

We get the CI configuration from the branch
[`ci-schedule-configuration`](https://gitlab.com/smondet/merbocop/commits/ci-schedule-configuration)
(it is used as a “moving pointer”).
See in the corresponding
[`.gitlab-ci.yml`](https://gitlab.com/smondet/merbocop/blob/ci-schedule-configuration/.gitlab-ci.yml) the jobs using the `<<: *full_cron_triggered` template.


But the `merbocop` code that actually runs comes from previously built docker
images, e.g.:

```yaml
  cron_image: "${CI_REGISTRY_IMAGE}:e408c5f1-run"
  cron_test_image: "${CI_REGISTRY_IMAGE}:ec28e7dc-run"
```

`cron_test_image` is used to run on an inspection on
[smondet/tezos](https://gitlab.com/smondet/tezos) and to make the “full report”
[page](https://smondet.gitlab.io/merbocop/full-tezos-tezos.html) on
[tezos/tezos](https://gitlab.com/tezos/tezos) (i.e. this version will not be
posting/editing comments on tezos/tezos).

`cron_image` is used to run the *real/production* inspection on
[tezos/tezos](https://gitlab.com/tezos/tezos) (the one that users see).


The `makewebsite` and `pages` jobs also run in the cron-pipeline, which is how
the reports are put on the website,
e.g. <https://smondet.gitlab.io/merbocop/full-tezos-tezos.html>.

