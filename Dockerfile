FROM registry.gitlab.com/tezos/flextesa:master-setup
RUN sudo mkdir -p /rebuild
RUN sudo chown -R opam:opam /rebuild
WORKDIR /rebuild
ADD --chown=opam:opam . ./
RUN opam config exec -- dune build merbocop.exe
FROM alpine
RUN  apk update
RUN  apk add curl
COPY --from=0 /rebuild/_build/default/merbocop.exe /usr/bin/merbocop

